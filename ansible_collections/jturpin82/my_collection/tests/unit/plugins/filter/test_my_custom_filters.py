from __future__ import (absolute_import, division, print_function)
import unittest
from ansible_collections.jturpin82.my_collection.plugins.filter.my_custom_filters import split_ip


class TestMyFilter(unittest.TestCase):

    def test_filter_can_split_an_ip(self):

        # Given
        filter_input = "10.0.0.1"
        # When
        result = split_ip(filter_input)
        # Then
        self.assertEqual(result, ["10", "0", "0", "1"])
